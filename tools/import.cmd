# 此脚本用于导入vivo-game-center-spider的mongodb数据
# 使用前先修改路径为mongodb的bin目录
set path=%path%;D:\tools\MongoDB\Server\4.0\bin
mongoimport -d vivo-game-center-spider -c newgames db/newgames.json
mongoimport -d vivo-game-center-spider -c hot-ranks db/hot-ranks.json
mongoimport -d vivo-game-center-spider -c new-ranks db/new-ranks.json
mongoimport -d vivo-game-center-spider -c alone-ranks db/alone-ranks.json
mongoimport -d vivo-game-center-spider -c games db/games.json
mongoimport -d vivo-game-center-spider -c compr-ranks db/compr-ranks.json
mongoimport -d vivo-game-center-spider -c recommend-ranks db/recommend-ranks.json
pause