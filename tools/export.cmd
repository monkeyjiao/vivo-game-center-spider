# 此脚本用于导出vivo-game-center-spider的mongodb数据
# 使用前先修改路径为mongodb的bin目录
set path=%path%;D:\tools\MongoDB\Server\4.0\bin
mongoexport -d vivo-game -c newgames -o db/newgames.json
mongoexport -d vivo-game -c hot-ranks -o db/hot-ranks.json
mongoexport -d vivo-game -c new-ranks -o db/new-ranks.json
mongoexport -d vivo-game -c alone-ranks -o db/alone-ranks.json
mongoexport -d vivo-game -c games -o db/games.json
mongoexport -d vivo-game -c compr-ranks -o db/compr-ranks.json
mongoexport -d vivo-game -c recommend-ranks -o db/recommend-ranks.json
pause