// 启动服务器
(function () {
    const express = require('express');
    const http = require('http');
    const cfg = require('./config.json');
    const app = express();
    const task = require('./libs/task');
    const moment = require('moment');
    const scheduler = require('./libs/scheduler');

    const httpServer = http.createServer(app);
    httpServer.listen(cfg.svr.port, () => {
        console.log(new Date());
        console.log(`HTTP Server running on port ${cfg.svr.port}!`);
    });

    app.get('/', (req, res) => {
        res.send('hello, noob~');
    });
    // 请求导出数据
    app.get('/export', async (req, res) => {
        const qs = req.query;
        const date = qs.date ? qs.date : moment().format('YYYY-MM-DD');
        const type = qs.type ? qs.type : 'hot';
        switch (qs.name) {
            case 'rank': await task.exportRank(date, type);
                break;
            case 'allGames': await task.exportAllGames();
                break;
            case 'newGames': await task.exportNewGames();
                break;
            default:
                break;
        }
        res.send(`指令收到了, export name: ${qs.name} , type: ${type}, date: ${date}`);
    });

    // 请求爬取数据
    app.get('/spider', async (req, res) => {
        const qs = req.query;
        switch (qs.name) {
            case 'everyDay': await task.everyDaySpider();
                break;
            case 'allGames': await task.getAllGames();
                break;
            case 'newGames': await task.spiderNewGames();
                break;
            case 'newGamesNow': 
                await task.getAllGames();
                await task.spiderNewGames();
                const fileName = await task.exportNewGames();
                await task.sendMail(fileName);
                break;
            case 'sendMail': 
                // await task.sendMail('newgame.20190305.csv');
                break;
            default:
                break;
        }
        res.send(`指令收到了, spider name: ${qs.name}`);
    });

    // 链接数据库
    const Mongo = require('./libs/mongodb');
    const mongoConfig = cfg.mongo;
    async function main() {
        console.log('start connect mongodb');
        await Mongo.connect(mongoConfig.uri, mongoConfig.opts);
        console.log('over connect mongodb');
        // 每周爬取 全部游戏
        scheduler.on(cfg.allGamesRule, async () => {
            await task.getAllGames();
        });
        // 每日爬取改为每周爬取
        scheduler.on(cfg.dailyRule, async () => {
            await task.everyDaySpider();
            const fileName = await task.exportNewGames();
            await task.sendMail(fileName);
        });
    }
    try {
        main();
    } catch (error) {
        console.log(error);
    }
}())
