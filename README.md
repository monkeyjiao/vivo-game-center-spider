## VIVO游戏中心 - apk

### 1. 获取排行榜
- 排行榜类型获取链接： http://main.gamecenter.vivo.com.cn/clientRequest/launcherInfo?vivo_channel=store&appVersion=38
- 链接: http://main.gamecenter.vivo.com.cn/clientRequest/rankList?page_index=1&type=hot&appVersion=38
- 参数：type：hot | Alone20150916173741 | Compr20150916173717 | new
- 返回数据里有一个 hasNext 表示是否还有下一页
### 2. 获取应用详细信息
- 链接: http://info.gamecenter.vivo.com.cn/clientRequest/gameDetail?pkgName=com.yxhy.dftk.vivo&id=61970&appVersion=38
- 参数：带上id可以获取到推荐(随机推荐)相关的游戏

### 3.获取推荐页面榜单
- 链接：http://main.gamecenter.vivo.com.cn/clientRequest/recommendBottomList?page_index=1&type=list&appVersion=38
- 参数：type作为list
- 返回数据里有一个 hasNext 表示是否还有下一页
 
### 4.获取新游首发榜单
- 链接：http://main.gamecenter.vivo.com.cn/clientRequest/startingGame?page_index=1&appVersion=38
- 返回数据里有一个 hasNext 表示是否还有下一页

### 5.全部游戏的获取方案
- 链接 获取游戏分类的id: http://main.gamecenter.vivo.com.cn/clientRequest/categories?appVersion=38

- 链接 获取单个分类所有游戏: http://main.gamecenter.vivo.com.cn/clientRequest/typeGames?appVersion=38&page_index=15&id=55
- 参数：其中id表示分类的id, 由上一个链接可以获取得到。
- 返回数据里有一个 hasNext 表示是否还有下一页

## 注意问题
### 更新频率
- 全部游戏：每周一次
- 4个排行榜：每天  爬取到的游戏都更新一次信息
- 新游首发：每天   爬取到的游戏都更新一次信息
- 首页推荐：每天   爬取到的游戏都更新一次信息

## 每日记录数据
- 评分
- 评论
- 下载
- 游戏名称
- 更新日期
- 版本号(versionCode)

## 特有参数记录
### 1.推荐页面
- fastGame（猜测是快游戏标签）
- gift
- activity
- threeDimension
- appId
- payType
- price
- burst
- arCore
- recommendType
- itemViewType
- traceData

### 2.新游首发
- fastGame
- gift
- activity
- threeDimension
- beta
- appId
- payType
- price
- burst
- arCore
- recommendType
- itemViewType
- categoryType
- categoryColorType

### 3.排行榜
#### hotRank | newRank | aloneRank | comprRank
- fastGame
- gift
- activity
- threeDimension  
- appId
- payType
- price                         
- burst
- arCore
- moduleInfo

