var mongoose = require('mongoose');

class Mongo {
	constructor() {

	}

	/**
	 * 连接mongodb数据库
	 * @param {String} uri - 连接 格式为mongodb://主机/数据库名
	 * @param {Object} opts - 链接字
	 */
	static connect(uri, opts) {
		return new Promise((resolve, reject) => {
			mongoose.connect(uri, opts, function (err) {
				if (err) {
					reject(err);
				} else {
					resolve(null);
				}
			});
		});
	}
}

module.exports = Mongo;