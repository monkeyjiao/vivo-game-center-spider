const Axios = require('axios');
/** 请求链接 */
async function req(url) {
    return await Axios.get(url).then((response) => {
        return response.data;
    }).catch((err) => {
        console.log(`req [${url}] error : ${err.message}`);
        return null;
    })
}
/** 获取单个游戏的详细信息 */
async function getDetailInfo(pkgName, id) {
    const url = `http://info.gamecenter.vivo.com.cn/clientRequest/gameDetail?pkgName=${pkgName}&id=${id}&appVersion=38`;
    const data = await req(url);
    return data;
}
/** 获取排行榜页面4个榜单 */
async function getRankList(type, pageId) {
    console.log(`start get RankList type = ${type}, pageId = ${pageId}`);
    const url = `http://main.gamecenter.vivo.com.cn/clientRequest/rankList?page_index=${pageId}&type=${type}&appVersion=38`;
    const data = await req(url);
    let arr = data.msg;
    pageId = data.current_page + 1;
    if (data.hasNext) {
        const tempArr = await getRankList(type, pageId);
        arr = arr.concat(tempArr);
    }
    return arr;
}
/** 获取推荐页面榜单 */
async function getRecommendList(pageId) {
    console.log(`start get recommendBottomList pageId = ${pageId}`);
    const url = `http://main.gamecenter.vivo.com.cn/clientRequest/recommendBottomList?page_index=${pageId}&type=list&appVersion=38`;
    const data = await req(url);
    let arr = data.app;
    pageId = data.current_page + 1;
    if (data.hasNext) {
        const tempArr = await getRecommendList(pageId);
        arr = arr.concat(tempArr);
    }
    return arr;
}
/** 获取新游首发榜单 */
async function getNewGameList(pageId) {
    console.log(`start get NewGameList pageId = ${pageId}`);
    const url = `http://main.gamecenter.vivo.com.cn/clientRequest/startingGame?page_index=${pageId}&appVersion=38`;
    const data = await req(url);
    let arr = data.msg;
    pageId = data.current_page + 1;
    if (data.hasNext) {
        const tempArr = await getNewGameList(pageId);
        arr = arr.concat(tempArr);
    }
    return arr;
}

/** 排行榜获取入口 */
async function getListByType(type) {
    switch (type) {
        case 'hot':
        case 'new':
        case 'Alone20150916173741':
        case 'Compr20150916173717':
            return await getRankList(type, 1);
        case 'Recommend':
            return await getRecommendList(1);
        case 'Newgame':
            return await getNewGameList(1);
        default:
            return [];
    }
}
/** 获取游戏所有的分类 */
async function getCategories() {
    console.log('start get Categries of all games');
    const url = 'http://main.gamecenter.vivo.com.cn/clientRequest/categories?appVersion=38';
    const data = await req(url);
    return data.msg;
}
/** 获取单个分类的游戏 */
async function getSingleCategoryGames(pageId, id) {
    const url = `http://main.gamecenter.vivo.com.cn/clientRequest/typeGames?appVersion=38&page_index=${pageId}&id=${id}`;
    const data = await req(url);
    let arr = data.msg;
    pageId = data.current_page + 1;
    if (data.hasNext) {
        const tempArr = await getSingleCategoryGames(pageId, id);
        arr = arr.concat(tempArr);
    }
    return arr;
}

exports.getDetailInfo = getDetailInfo;
exports.getListByType = getListByType;
exports.getCategories = getCategories;
exports.getSingleCategoryGames = getSingleCategoryGames;