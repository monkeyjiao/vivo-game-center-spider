const express = require('express');
const http = require('http');
const cfg = require('../config.json');

// 启动服务器
const app = express();
const httpServer = http.createServer(app);
httpServer.listen(cfg.svr.port, () => {
    console.log(new Date());
    console.log(`HTTP Server running on port ${cfg.svr.port}!`);
});


// 配置一些api用于手动调用
app.get('/', (req, res) => {
    res.send('恭喜你，找到了后门！');
});

app.get('/task/', (req, res) => {
    task();
    res.send('恭喜你，找到了后门！');
});

app.get('/cancel', (req, res) => {
    job.cancel();
    res.send('恭喜你，找到了后门！');
});

class Svr {
    _app = null;
    _httpServer = null;
    constructor() {
        this._app = express();
        this._httpServer = http.createServer(app);
    }

    async init(port) {
        return new Promise((resolve, reject) => {
            this._httpServer.listen(port)
            return app;
        });
    }
}
