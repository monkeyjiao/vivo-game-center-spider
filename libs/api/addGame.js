const { Game } = require('../model/index');

module.exports = function addGame(game, cb) {
    Game.findOne({ id: game.id }, function (err, doc) {
        if (err) return console.error(err);
        if (!doc) { // 新增游戏信息
            doc = new Game(game);
        } else { // 刷新游戏信息
            doc.modifyTime = Date.now();
            doc.size = game.size;
            doc.pkgName = game.pkgName;
            doc.icon = game.icon;
            doc.apkUrl = game.apkUrl;
            doc.comment = game.comment;
            doc.commentNum = game.commentNum;
            doc.download = game.download;
            doc.date = game.date;
            doc.versonCode = game.versonCode;
            doc.versonName = game.versonName;
            doc.type = game.type;
            doc.recommend_desc = game.recommend_desc;
            doc.categoryId = game.categoryId;
            doc.editor = game.editor;
            doc.net_game = game.net_game;
            doc.fitModel = game.fitModel;
            doc.showType = game.showType;
            doc.gameTag = game.gameTag;
            doc.first_pub = game.first_pub;
            doc.support_msg = game.support_msg;
            doc.screenshot = game.screenshot;
            doc.gameDeveloper = game.gameDeveloper;
            doc.servicePhone = game.servicePhone;
        }
        doc.save(cb);
    });
};