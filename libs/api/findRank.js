const { getListModel } = require('../model/index');

module.exports = async function findRank(date, type, cb) {
    const RankModel = getListModel(type);
    return new Promise((resolve) => {
        RankModel.findOne({ date: date }, (err, doc) => {
            if (err) {
                console.error(err);
                resolve([]);
                return;
            }
            resolve(doc.toJSON().list);
        });
    });
};