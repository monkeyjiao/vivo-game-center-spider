const {
    getListModel
} = require('../model/index');

module.exports = async function findRankArr(type, selection, cb) {
    const RankModel = getListModel(type);
    return new Promise((resolve) => {
        RankModel.find({}, selection, (err, doc) => {
            if (err) {
                console.error(err);
                resolve([]);
                return;
            }
            if (!doc) {
                resolve([]);
                return;
            } else {
                let games = []
                if (doc.length > 0) {
                    for (let i = 0; i < doc.length; i++) {
                        let gameObj = doc[i];
                        games[i] = gameObj.toObject();
                    }
                }
                resolve(games);
            }
        });
    });
};