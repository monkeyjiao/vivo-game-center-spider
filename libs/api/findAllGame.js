const { Game } = require('../model/index');

module.exports = function findAllGame(id, select, cb) {
    if (select instanceof Function) {
        cb = select
        select = '';
    }
    return new Promise((resolve) => {
        Game.find((err, res) => {
            if (err) {
                console.error(err);
                resolve([]);
                return;
            }
            let arr = [];
            res.map(async (item, i) => arr[i] = item.toJSON());
            resolve(arr);
        });
    });
};