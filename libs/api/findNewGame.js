const { NewGame } = require('../model/index');

module.exports = function findGame(select, cb) {
    if (select instanceof Function) {
        cb = select
        select = '';
    }
    return new Promise((resolve) => {
        NewGame.find({}, select, (err, res) => {
            if (err) {
                console.error(err);
                resolve([]);
                return;
            }
            let arr = [];
            res.map((item, i) => { arr[i] = item.toJSON(); })
            resolve(arr)
        });
    });
};