const { Game } = require('../model/index');

module.exports = function findGame(id, select, cb) {
    if (select instanceof Function) {
        cb = select
        select = '';
    }
    return new Promise((resolve) => {
        Game.findOne({ id: id }, select, (err, doc) => {
            if (err) {
                console.error('error: ' + err.message);
                resolve({});
                return;
            }
            if (!doc) {
                console.warn('id = ' + id + ' is not find!');
                resolve({});
                return;
            }
            resolve(doc.toJSON())
        });
    });
};