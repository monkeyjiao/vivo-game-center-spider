var moment = require('moment');
var { getListModel } = require('../model/index');

module.exports = function addRank(arr, type, cb) {
    const RankModel = getListModel(type);
    const date = moment().format('YYYY-MM-DD');
    RankModel.findOne({ date: date }, function(err, doc) {
        if (err) return cb(err);
        if (!doc) {
            doc = new RankModel({ list: arr });
            doc.date = date;
        } else {
            doc.list = arr;
        }
        doc.save(cb);
    });
};