const { NewGame } = require('../model/index');

module.exports = function addNewGame(game, cb) {
    const arr = game.categoryType.match(/\d+/ig);
    arr[0] = arr[0].length < 2 ? ('0' + arr[0]) : arr[0];
    arr[1] = arr[1].length < 2 ? ('0' + arr[1]) : arr[1];
    let year = new Date().getFullYear();
    const curMonth = new Date().getMonth() + 1;
    if (curMonth === 1 && Number(arr[0]) == 12) {
        year = year - 1;
    }
    game.categoryType = year + arr[0] + arr[1];
    NewGame.findOne({
        id: game.id
    }, function (err, doc) {
        if (err) return console.error(err);
        if (!doc) { // 新增游戏信息
            doc = new NewGame(game);
        } else { // 刷新游戏信息
            doc.download = game.download;
            doc.comment = game.comment;
            doc.commentNum = game.commentNum;
            doc.versionCode = game.versionCode;
            doc.fastGame = game.fastGame;
            doc.gift = game.gift;
            doc.activity = game.activity;
            doc.threeDimension = game.threeDimension;
            doc.beta = game.beta;
            doc.appId = game.appId;
            doc.payType = game.payType;
            doc.price = game.price;
            doc.burst = game.burst;
            doc.arCore = game.arCore;
            doc.recommendType = game.recommendType;
            doc.itemViewType = game.itemViewType;
            doc.categoryType = game.categoryType;
            doc.modifyTime = Date.now();
        }
        doc.save(cb);
    });
};