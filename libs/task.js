const addGame = require('./api/addGame');
const addRank = require('./api/addRank');
const addNewGame = require('./api/addNewGame');
const findRank = require('./api/findRank');
const findNewGame = require('./api/findNewGame');
const findGame = require('./api/findGame');
const findAllGame = require('./api/findAllGame');
const findRankArr = require('./api/findRankArr');
const spider = require('./spider/spider');
const utils = require('./utils');
const _ = require('lodash');
const path = require('path');
const moment = require('moment');
const cfg = require('../config.json');
const mailer = require('./mailer');

/**
 * 爬取排行榜数据
 * @param {*} type new | hot | Alone20150916173741 | Compr20150916173717 | Recommend
 */
async function spiderRankList(type) {
    const rankData = await spider.getListByType(type);
    // await getGameDetailByArray(rankData);
    return new Promise((resolve) => {
        addRank(rankData, type, (err) => {
            if (err) return console.log(err);
            console.log(`addRank ${type} over`);
            resolve();
        });
    });
}

/** 每日爬取的排行榜 */
async function everyDaySpider() {
    await spiderRankList('hot');
    await spiderRankList('new');
    await spiderRankList('Alone20150916173741');
    await spiderRankList('Compr20150916173717');
    await spiderRankList('Recommend');
    await spiderNewGames();
}
/** 爬取新游戏 */
async function spiderNewGames() {
    console.log('start spiderNewGames');
    const rankData = await spider.getListByType('Newgame');
    if (!rankData) return;
    console.log(`start add newGame len = ${rankData.length}`);
    for (let i = 0; i < rankData.length; i++) {
        const item = rankData[i];
        await new Promise((resolve) => {
            addNewGame(item, (err) => {
                if (err) console.log(err);
                resolve();
            });
        });
    }

    // let obj = {};
    const rankArr = await findNewGame('id name download commentNum');
    // for (let j = 0; j < rankArr.length; j++) {
    //     const game = await new Promise.resolve(rankArr[j]);
    //     obj[game.id] = game;
    // }
    addRank(rankArr, 'newgame', (err) => {
        if (err) return console.log(err);
        console.log(`addRank newgame over`);
    });
    console.log(`over add newGame len = ${rankData.length}`);
    console.log('over spiderNewGames');
}
/** 获取游戏信息通过数组 */
async function getGameDetailByArray(arr) {
    for (let i = 0; i < arr.length; i++) {
        const game = arr[i];
        await getGameDetail(game.pkgName, game.id);
    }
}
/** 获取游戏详细信息 */
async function getGameDetail(pkgName, id) {
    const data = await spider.getDetailInfo(pkgName, id);
    if (!data || !data.game) return;
    let game = data.game;
    game.picDesc = data.hasOwnProperty('picDesc') ? data.picDesc : [];
    game.newserver = data.hasOwnProperty('newserver') ? data.newserver : [];
    game.betatest = data.hasOwnProperty('betatest') ? data.betatest : [];
    game.tag = data.hasOwnProperty('tag') ? data.tag : [];
    return new Promise((resolve) => {
        // console.log(`start addGame pkgName = ${pkgName}, id = ${id}`);
        addGame(game, (err) => {
            if (err) return console.log(err);
            // console.log(`over addGame pkgName = ${pkgName}, id = ${id}`);
            resolve();
        });
    })
}
/** 爬取所有游戏 */
async function getAllGames() {
    console.log('start get All Games');
    const arr = await spider.getCategories();
    for (let i = 0; i < arr.length; i++) {
        const item = arr[i];
        const id = item.id;
        const name = item.name;
        const num = item.gameNum;
        console.log(`start get the id: ${id}, type: ${name}, num: ${num}`);
        const singleCategoryGames = await spider.getSingleCategoryGames(1, id);
        console.log('singleCategoryGames.len = ' + singleCategoryGames.length);
        await getGameDetailByArray(singleCategoryGames);
        console.log(`end get the id: ${id}, type: ${name}, num: ${num}`);
    }
    console.log('over get All Games');
}
/** 导出所有游戏 */
async function exportAllGames() {
    console.log('start export All Games');
    const tempFile = path.join(__dirname, '../temp/allGames.csv');
    const outFile = path.join(__dirname, `../out/allGames.${moment().format('YYYYMMDD')}.csv`);
    let arr = await findAllGame();
    await utils.exportJsonToCsv(tempFile, arr);
    await utils.utf8ToGbk(tempFile, outFile, () => console.log('DONE!'));
    console.log('over export All Games');
}
/** 导出排行榜 */
async function exportRank(date, type) {
    console.log(`start export rank: ${type}`);
    const tempFile = path.join(__dirname, `../temp/${type}-rank.csv`);
    const outFile = path.join(__dirname, `../out/${type}-rank.${moment(date).format('YYYYMMDD')}.csv`);
    let arr = await findRank(date, type);
    for (let i = 0; i < arr.length; i++) {
        const item = arr[i];
        for (const key in item) {
            if (item.hasOwnProperty(key)) {
                const element = item[key];
                if (typeof (element) == 'object') {
                    item[key] = JSON.stringify(element);
                }
            }
        }
    }
    await utils.exportJsonToCsv(tempFile, arr);
    await utils.utf8ToGbk(tempFile, outFile, () => console.log('DONE!'));
    console.log(`over export rank: ${type}`);
}
/** 导出所有排行榜 */
async function exportAllRanks(date) {
    await exportRank(date, 'hot');
    await exportRank(date, 'new');
    await exportRank(date, 'alone');
    await exportRank(date, 'compr');
    await exportRank(date, 'recommend');
}
/** 导出新游首发榜单 */
async function exportNewGames(date) {
    console.log('start export New Games');
    const tempFile = path.join(__dirname, `../temp/newgame.csv`);
    const outFile = path.join(__dirname, `../out/newgame.${moment(date).format('YYYYMMDD')}.csv`);
    let newDate = await findNewGame();
    let arr = await addParamsForNewGames(newDate);
    console.log('arr.length = ' + arr.length);
    await utils.exportJsonToCsv(tempFile, arr);
    await utils.utf8ToGbk(tempFile, outFile, () => console.log('DONE!'));
    console.log('over export New Games');
    return outFile;
}

// 计算下载量和评论数
async function addDownloadAndCommentNum() {
    const rankArr = await findRankArr('newgame', 'date list');
    rankArr.sort((a, b) => moment(b.date) - moment(a.date));
    const curWeekArr = rankArr[0].list;
    let lastWeekArr = [];
    if (!rankArr[1] || !Object.hasOwnProperty(rankArr[1], 'list')) {
        lastWeekArr = [];
    } else {
        lastWeekArr = rankArr[1].list;
    }
    
    let curWeekGames = await utils.arrToObj(curWeekArr);
    let lastWeekGames = await utils.arrToObj(lastWeekArr);
    let pArr = [];
    const keyArr = Object.keys(curWeekGames);
    for (let j = 0; j < keyArr.length; j++) {
        const id = keyArr[j];
        let game = curWeekGames[id];
        const lastWeekGame = lastWeekGames[id];
        if (!lastWeekGame) {
            game.lastWeekDownLoad = 0;
            game.lastWeekCommentNum = 0;
        } else {
            game.lastWeekDownLoad = lastWeekGame.download;
            game.lastWeekCommentNum = lastWeekGame.commentNum;
        }
        game.deltaDownLoad = game.download - game.lastWeekDownLoad;
        game.deltaCommentNum = game.commentNum - game.lastWeekDownLoad;
        pArr.push(Promise.resolve);
    }
    return Promise.all(pArr).then(() => {
        console.log('over addDownloadAndCommentNum');
        return curWeekGames;
    }).catch((err) => {
        console.log(err.message);
        return curWeekGames;
    });

}

/** 增加新的参数 */
async function addParamsForNewGames(arr) {
    const addGames = await addDownloadAndCommentNum();
    let pArr = [];
    let newArr = [];
    for (let i = 0; i < arr.length; i++) {
        const game = arr[i];
        const id = game.id;
        // 上线时间
        const today = moment().format('YYYYMMDD');
        const publishDay = moment(game.categoryType);
        game.onlineDays = (moment(today) - moment(publishDay)) / (60 * 60 * 24 * 1000);
        // 日均新增
        game.dnu = game.onlineDays == 0 ? game.download : parseInt(game.download / game.onlineDays);
        // 其他参数
        const selection = 'screenshot new_gift_tag gameTag gameDeveloper exist_forum desc date';
        let info = await findGame(id, selection);
        let addInfo = addGames[id];
        if (_.isEmpty(info)) {
            info = {
                screenshot: '',
                new_gift_tag: '',
                gameTag: '',
                gameDeveloper: '',
                exist_forum: '',
                desc: '',
                date: ''
            }
        }
        if (_.isEmpty(addInfo)) {
            addInfo = {
                lastWeekDownLoad: 0,
                lastWeekCommentNum: 0,
                deltaDownLoad: game.download,
                deltaCommentNum: game.commentNum
            }
        }
        newArr[i] = await _.merge(game, info, addInfo);
        pArr.push(Promise.resolve);
    }
    return Promise.all(pArr).then(() => {
        return newArr;
    }).catch(() => {
        console.error(err.message);
        return newArr;
    });
}


/** 发送邮件 */
async function sendMail(csvFile) {
    console.log('== start sendMail ==');
    console.log(new Date());
    const time = csvFile.replace(/\D+/ig, '');
    let mailOptions = cfg.mailOptions;
    mailOptions.subject = `vivo每周新游(${moment(time).format('YYYY年MM月DD日')})`;
    mailOptions.attachments = [
        { filename: path.basename(csvFile), path: path.join('out', path.basename(csvFile)) }
    ];
    mailer.sendMail(mailOptions);
    console.log('== end sendMail ==');
    console.log(new Date());
}

exports.everyDaySpider = everyDaySpider;
exports.getAllGames = getAllGames;
exports.exportAllGames = exportAllGames;
exports.exportRank = exportRank;
exports.exportAllRanks = exportAllRanks;
exports.exportNewGames = exportNewGames;
exports.spiderNewGames = spiderNewGames;
exports.sendMail = sendMail;