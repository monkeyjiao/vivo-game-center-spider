const mongoose = require('mongoose');
const GameSchema = require('./schema-game');
const NewGameSchema = require('./schema-newgame');
const getListSchema = require('./get-list-schema');

const md = {
    'hot-rank': mongoose.model('hot-rank', getListSchema('hot')),
    'new-rank': mongoose.model('new-rank', getListSchema('new')),
    'alone-rank': mongoose.model('alone-rank', getListSchema('alone')),
    'compr-rank':mongoose.model('compr-rank', getListSchema('compr')),
    'recommend-rank': mongoose.model('recommend-rank', getListSchema('recommend')),
    'newgame-rank': mongoose.model('newgame-rank', getListSchema('newgame'))
}
function getListModel(rankType) {
    console.log('rankType = ' + rankType + ' , type = ' + typeof(rankType));
    rankType = rankType.replace(/\d+/ig, '');
    rankType = rankType.toLocaleLowerCase();
    return md[`${rankType}-rank`];
}
exports.getListModel = getListModel;

exports.Game = mongoose.model('Game', GameSchema);
exports.NewGame = mongoose.model('NewGame', NewGameSchema);
// exports.Rank = mongoose.model('Rank', RankSchema);
// exports.Recommend = mongoose.model('Recommend', RecommendSchema);