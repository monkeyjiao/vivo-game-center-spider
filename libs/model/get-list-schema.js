const mongoose = require('mongoose');
const mToObject = require('mongoose-toobject');
const Schema = mongoose.Schema;
const RankSchema = require('./schema-rank');
const NewGameCountRankSchema = require('./schema-newgame-count-rank');
const RecommendSchema = require('./schema-recommend');

function getListSchema(rankType) {
    let subSchema = null;
    switch (rankType) {
        case 'hot':
        case 'new':
        case 'alone':
        case 'compr':
            subSchema = RankSchema;
            break;
        case 'recommend':
            subSchema = RecommendSchema;
            break;
        case 'newgame':
            subSchema = NewGameCountRankSchema;
            break;
    }

    const ListSchema = new Schema({
        createTime: { type: Date, default: Date.now },  // 创建时间
        list: [subSchema], // 排行榜;
        date: String, // 排行榜日期：2018-09-12
    });
    ListSchema.plugin(mToObject, { hide: '__v _id' });
    ListSchema.index({
        createTime: 1
    });
    return ListSchema;
}

module.exports = getListSchema;
