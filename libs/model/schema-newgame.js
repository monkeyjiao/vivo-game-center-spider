const mongoose = require('mongoose');
const mToObject = require('mongoose-toobject');
const Schema = mongoose.Schema;

const NewGameSchema = new Schema({
    id: Number,
    name: String, // 游戏名称
    date: String, // 更新日期
    download: String, // 下载人数
    comment: Number,  // 评分
    commentNum: Number, // 评论人数
    versionCode: String, // 安卓版本号
    fastGame: Number,
    gift: String,
    activity: String,
    threeDimension: String,
    beta: String,
    appId: Number,
    payType: Number,
    price: Number,
    burst: Number,
    arCore: Number,
    recommendType: String,
    itemViewType: String,
    categoryType: String, // 日期类型
    // categoryColorType: Number,
    createTime: { type: Date, default: Date.now },  // 创建时间
    modifyTime: { type: Date, default: Date.now },  // 修改时间
});

NewGameSchema.plugin(mToObject, { hide: '__v _id' });
NewGameSchema.index({
    createTime: 1
});

module.exports = NewGameSchema;