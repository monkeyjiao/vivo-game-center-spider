const mongoose = require('mongoose');
const mToObject = require('mongoose-toobject');
const Schema = mongoose.Schema;

const RecommendSchema = new Schema({
    id: Number,
    name: String, // 游戏名称
    date: String, // 更新日期
    download: String, // 下载人数
    comment: Number,  // 评分
    commentNum: Number, // 评论人数
    versionCode: Number, // 安卓版本号
    rank: Number, // 排行
    fastGame: Number,
    gift: String,
    activity: String,
    threeDimension: String,
    appId: Number,
    payType: Number,
    price: Number,
    burst: Number,
    arCore: Number,
    recommendType: String,
    itemViewType: String,
    traceData: {
        recommendDate : String,
        sRecommendIndex: Number,
    },
    createTime: { type: Date, default: Date.now },  // 创建时间
    modifyTime: { type: Date, default: Date.now },  // 修改时间
});

RecommendSchema.plugin(mToObject, { hide: '__v _id' });
RecommendSchema.index({
    createTime: 1
});

module.exports = RecommendSchema;
