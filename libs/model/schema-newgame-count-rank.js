const mongoose = require('mongoose');
const mToObject = require('mongoose-toobject');
const Schema = mongoose.Schema;

const NewGameCountRankSchema = new Schema({
    id: Number,
    name: String, // 游戏名称
    download: String, // 下载数量
    commentNum: Number, // 评论数量
});

NewGameCountRankSchema.plugin(mToObject, { hide: '__v _id' });
NewGameCountRankSchema.index({
    id: 1
});

module.exports = NewGameCountRankSchema;