const mongoose = require('mongoose');
const mToObject = require('mongoose-toobject');
const Schema = mongoose.Schema;

const RankSchema = new Schema({
    id: Number,
    name: String, // 游戏名称
    date: String, // 更新日期
    download: String, // 下载人数
    comment: Number,  // 评分
    commentNum: Number, // 评论人数
    versionCode: Number, // 安卓版本号
    rank: Number, // 排行
    fastGame: Number,
    gift: String,
    activity: String,
    threeDimension: String,
    appId: Number,
    payType: Number,
    price: Number,
    burst: Number,
    arCore: Number,
    moduleInfo: String,
    createTime: { type: Date, default: Date.now },  // 创建时间
    modifyTime: { type: Date, default: Date.now },  // 修改时间
});

RankSchema.plugin(mToObject, { hide: '__v _id' });
RankSchema.index({
    createTime: 1
});

module.exports = RankSchema;