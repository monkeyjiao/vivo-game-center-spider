const mongoose = require('mongoose');
const mToObject = require('mongoose-toobject');
const Schema = mongoose.Schema;

const GameSchema = new Schema({
    id: Number,
    name: String,
    size: Number, 
    pkgName: String, 
    icon: String,
    apkurl: String, // apk包下载地址
    comment: Number, // 评分
    commentNum: Number, // 评论人数
    download: Number, // 下载人数
    versonCode: String, 
    versonName: String, // 安卓版本号
    type: String,
    recommend_desc: String,
    categoryId: String,
    editor: String,
    net_game: String, // 是否网游
    fitModel: Boolean,
    showType: String,
    gameTag: String,
    first_pub: String,
    support_msg: String,
    screenshot: String,
    flag: String,
    gift_size: Number,
    new_gift_size: Number,
    new_gift_tag: Boolean,
    exist_forum: Boolean,
    date: String, // 更新时间
    desc: String,
    gift_title: String,
    safe: Number,
    advertise: Number,
    gameDeveloper: String,
    official: Number,
    servicePhone: String,
    serviceQQ: String,
    picDesc: { type: Array, default: [] },
    newserver: { type: Array, default: [] },
    betatest: { type: Array, default: [] },
    tag: { type: Array, default: [] },
    createTime: { type: Date, default: Date.now },  // 创建时间
    modifyTime: { type: Date, default: Date.now },  // 修改时间
});

GameSchema.plugin(mToObject, { hide: '__v _id' });
GameSchema.index({
    createTime: 1
});

module.exports = GameSchema;